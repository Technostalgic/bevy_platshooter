use std::fmt::Debug;

use bevy::{input::InputSystem, prelude::*, utils::HashMap};

use super::super::ControllerSystem;
use super::action::Action;

// Player Input System Set: ------------------------------------------------------------------------

/// a system set that runs in preupdate after [`bevy::input::InputSystem`], a more
/// precise subset of [`ControllerSystem`] which only contains systems that are dealing directly
/// with input in a way directly related to the [`crate::player`] module
#[derive(SystemSet, Debug, Hash, Clone, Copy, PartialEq, Eq)]
pub(crate) struct PlayerInputSystem;

// Player Plugin: ----------------------------------------------------------------------------------

pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.configure_sets(PreUpdate, ControllerSystem.after(InputSystem))
            .insert_resource(Players::default())
            .add_systems(
                PreUpdate,
                handle_player_input
                    .in_set(PlayerInputSystem)
                    .in_set(ControllerSystem),
            );
    }
}

// Current Players: --------------------------------------------------------------------------------

/// a resource used to hold the set of all current players in the game
#[derive(Resource, Debug)]
pub struct Players {
    pub current_players: Vec<Player>,
}

impl Default for Players {
    fn default() -> Self {
        Self {
            current_players: vec![Player::default()],
        }
    }
}

/// a container used to store all the necessary information about a user currently playing the game
#[derive(Default)]
pub struct Player {
    pub id: usize,
    pub input_config: PlayerInputConfig,
    current_actions: Vec<Action>,
}

impl Debug for Player {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Player").field("id", &self.id).finish()
    }
}

impl Player {
    pub fn current_actions(&self) -> &Vec<Action> {
        &self.current_actions
    }
}

// Player Input: -----------------------------------------------------------------------------------

/// information about what inputs correspond to what actions for the player to make
#[derive(Debug, Clone)]
pub struct PlayerInputConfig {
    pub bindings: HashMap<PlayerInput, Vec<Action>>,
}

impl Default for PlayerInputConfig {
    fn default() -> Self {
        Self {
            bindings: HashMap::from([
                (
                    PlayerInput::Key(KeyCode::D),
                    vec![Action::MoveHorizontal(1.0.into())],
                ),
                (
                    PlayerInput::Key(KeyCode::A),
                    vec![Action::MoveHorizontal((-1.0).into())],
                ),
                (
                    PlayerInput::Key(KeyCode::W),
                    vec![Action::MoveVertical(1.0.into())],
                ),
                (
                    PlayerInput::Key(KeyCode::S),
                    vec![Action::MoveVertical((-1.0).into())],
                ),
                (PlayerInput::Key(KeyCode::Space), vec![Action::Jump]),
                (PlayerInput::Key(KeyCode::E), vec![Action::Interact]),
                (
                    PlayerInput::MouseButton(MouseButton::Left),
                    vec![Action::Attack],
                ),
            ]),
        }
    }
}

/// a type used to describe a single input that can be linked to an action
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub enum PlayerInput {
    Key(KeyCode),
    MouseButton(MouseButton),
}

// Systems: ----------------------------------------------------------------------------------------

/// adds current actions to the players based on key/mouse input
fn handle_player_input(
    mut players: ResMut<Players>,
    keyboard_input: Res<Input<KeyCode>>,
    mouse_btn_input: Res<Input<MouseButton>>,
) {
    // iterate through each player in the game
    for player in &mut players.current_players {
        player.current_actions.clear();
        // add all actions from keyboard input bindings
        for key in keyboard_input.get_pressed() {
            if let Some(actions) = player.input_config.bindings.get(&PlayerInput::Key(*key)) {
                for action in actions {
                    player.current_actions.push(*action);
                }
            }
        }
        // add all actions from mouse button bindings
        for btn in mouse_btn_input.get_pressed() {
            if let Some(actions) = player
                .input_config
                .bindings
                .get(&PlayerInput::MouseButton(*btn))
            {
                for action in actions {
                    player.current_actions.push(*action);
                }
            }
        }
    }
}
