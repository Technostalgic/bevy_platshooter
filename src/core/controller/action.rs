use std::{hash::Hash, ops::Neg};

use bevy::{prelude::*, utils::HashSet};

use super::player::Players;

// Action Plugin: ----------------------------------------------------------------------------------

/// plugin that handles setting up action types and systems related to them
pub(crate) struct ActionPlugin;

impl Plugin for ActionPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, handle_action_receivers);
    }
}

// Scalar: -----------------------------------------------------------------------------------------

/// basically an alias for f32 except I needed to be hashable and implement eq, so I just treat
/// NaN values as equal to each other even if that's not quite mathematically correct
#[derive(Deref, DerefMut, Debug, Clone, Copy)]
pub struct Scalar(pub f32);

impl Hash for Scalar {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        // since the bit representation of NaN can be different for different NaN instances, and
        // we want all NaN values to have the same hash, we must first check to see if the scalar
        // is NaN, and if it is, hash the same NaN value each time
        if self.0.is_nan() {
            f32::NAN.to_bits().hash(state);
        } else {
            self.0.to_bits().hash(state);
        }
    }
}

impl PartialEq for Scalar {
    fn eq(&self, other: &Self) -> bool {
        if self.0.is_nan() {
            other.is_nan()
        } else if other.0.is_nan() {
            self.is_nan()
        } else {
            self.0 == other.0
        }
    }
}

impl Eq for Scalar {}

impl From<f32> for Scalar {
    fn from(value: f32) -> Self {
        Self(value)
    }
}

impl Neg for Scalar {
    type Output = Scalar;
    fn neg(self) -> Self::Output {
        Self(-self.0)
    }
}

// Action: -----------------------------------------------------------------------------------------

/// a type used to describe the player's intent when they trigger a control
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub enum Action {
    MoveHorizontal(Scalar),
    MoveVertical(Scalar),
    Jump,
    Interact,
    Attack,
}

// Action Receiver: --------------------------------------------------------------------------------

/// a component that receives and stores actions performed by the specified player each frame
#[derive(Component, Default, Debug, Clone)]
pub struct ActionReceiver {
    /// which player's actions should be received
    pub player_id: usize,
    recieved_actions: HashSet<Action>,
}

impl ActionReceiver {
    pub fn recieved_actions(&self) -> &HashSet<Action> {
        &self.recieved_actions
    }
    pub fn send_action(&mut self, action: &Action) {
        self.recieved_actions.insert(*action);
    }
    pub fn clear_actions(&mut self) {
        self.recieved_actions.clear();
    }
}

fn handle_action_receivers(players: ResMut<Players>, mut query: Query<&mut ActionReceiver>) {
    let plr_count = players.current_players.len();
    for mut receiver in &mut query {
        if receiver.player_id < plr_count {
            for action in players.current_players[receiver.player_id].current_actions() {
                receiver.send_action(action);
            }
        }
    }
}
