use bevy::app::Plugin;

pub mod action;
pub mod player;
pub mod player_cam;

// Controller Plugin: ------------------------------------------------------------------------------

pub struct ControllerPlugin;

impl Plugin for ControllerPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_plugins((
            action::ActionPlugin,
            player::PlayerPlugin,
            player_cam::PlayerCamPlugin,
        ));
    }
}
