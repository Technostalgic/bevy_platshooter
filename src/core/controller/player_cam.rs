use super::action::ActionReceiver;
use bevy::{prelude::*, transform::TransformSystem};
use bevy_xpbd_2d::prelude::*;

// Player Cam Plugin: ------------------------------------------------------------------------------

pub struct PlayerCamPlugin;

impl Plugin for PlayerCamPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            PostUpdate,
            control_cameras.after(TransformSystem::TransformPropagate),
        );
    }
}

// Player Cam Controller Component: ----------------------------------------------------------------

#[derive(Component, Clone, Copy)]
pub struct PlayerCamController {
    /// the player that the camera should follow
    pub player_id: usize,
    /// how quickly the camera moves from it's current position to it's  target
    pub follow_factor: f32,
    /// how much the velocity effects the camera movement
    pub velocity_factor: f32,
    prev_position: Vec2,
}

impl Default for PlayerCamController {
    fn default() -> Self {
        Self {
            player_id: default(),
            follow_factor: 0.05,
            velocity_factor: 0.5,
            prev_position: default(),
        }
    }
}

/// a system to control the player cameras based on the players in game
pub fn control_cameras(
    time: Res<Time>,
    mut cam_query: Query<(&mut Transform, &mut PlayerCamController)>,
    player_query: Query<(&ActionReceiver, &GlobalTransform, Option<&LinearVelocity>)>,
) {
    let dt = time.delta_seconds();
    for (mut cam_trans, mut cam_ctrl) in &mut cam_query {
        let mut target_count = 0;
        let mut target_trans = Vec2::ZERO;
        let mut target_vel = Vec2::ZERO;
        for (hero_action, hero_glob_trans, hero_vel) in &player_query {
            // we only want to pay attention to heros of the same player id as the cam controller
            if hero_action.player_id != cam_ctrl.player_id {
                continue;
            }
            target_count += 1;
            target_trans += hero_glob_trans.translation().truncate();
            if let Some(hero_vel) = hero_vel {
                target_vel += hero_vel.0;
            }
        }
        // calculate average translation and velocity
        target_trans /= target_count as f32;
        target_vel /= target_count as f32;
        // begin calculating final camera position
        let target_pos = target_trans + target_vel * cam_ctrl.velocity_factor;
        // adjust follow factor for delta time
        let follow_adj = 1.0 - (1.0 - cam_ctrl.follow_factor).powf(dt * 60.0);
        let final_pos = cam_ctrl.prev_position * (1.0 - follow_adj) + (target_pos * follow_adj);
        cam_trans.translation = final_pos.extend(cam_trans.translation.z);
        cam_ctrl.prev_position = final_pos;
    }
}

// Player Cam Bundle: ------------------------------------------------------------------------------

#[derive(Bundle, Default)]
pub struct PlayerCamBundle {
    pub player_cam_ctrl: PlayerCamController,
    pub cam_bundle: Camera2dBundle,
}
