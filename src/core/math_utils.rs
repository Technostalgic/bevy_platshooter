use bevy::prelude::*;
use bevy_xpbd_2d::parry::na::Point2;

// Basic Math: -------------------------------------------------------------------------------------

/// perform a basic modulo operation between two numbers. this should be used over the % operator
/// because the % operator is finnicky in that it can sometimes return a negative result. don't use
/// a negative number for b
pub fn modulo(a: f32, b: f32) -> f32 {
    let result = a % b;
    if result < 0.0 {
        result + b
    } else {
        result
    }
}

pub trait IsZero {
    fn is_zero(&self) -> bool;
}

impl IsZero for Vec2 {
    fn is_zero(&self) -> bool {
        self.x == 0.0 && self.y == 0.0
    }
}

// Nalgebra Interopobility: ------------------------------------------------------------------------

pub trait ToFromPoint2 {
    /// convert this object to a point2 type compatible with the nalgebra library
    fn to_point2(&self) -> Point2<f32>;
    /// convert a nalgebra point2 object into this type
    fn from_point2(pt: &Point2<f32>) -> Self;
}

impl ToFromPoint2 for Vec2 {
    fn to_point2(&self) -> Point2<f32> {
        Point2::<f32>::new(self.x, self.y)
    }
    fn from_point2(pt: &Point2<f32>) -> Self {
        Self { x: pt.x, y: pt.y }
    }
}

// 2D Transformation: ------------------------------------------------------------------------------

pub trait TransformPoint2D {
    /// transform a 2d point to global space from local space
    fn transform_point2(&self, point_2d: Vec2) -> Vec2;
    /// transform a 2d point to local space from global space
    fn inverse_transform_point2(&self, point_2d: Vec2) -> Vec2;
    /// create a world space rect from two points in local space
    fn transform_rect_from_points(&self, local_pt_a: Vec2, local_pt_b: Vec2) -> Rect;
    /// create a local space rect from two points in global space
    fn inverse_transform_rect_from_points(&self, glob_pt_a: Vec2, glob_pt_b: Vec2) -> Rect;
    /// transforms point 0,0 and 1,1 and figures what the scale used to transform them was
    fn extract_transform_scale2(&self) -> Vec2;
}

impl TransformPoint2D for GlobalTransform {
    fn transform_point2(&self, point_2d: Vec2) -> Vec2 {
        self.transform_point(point_2d.extend(0.0)).truncate()
    }
    fn inverse_transform_point2(&self, point_2d: Vec2) -> Vec2 {
        self.compute_matrix()
            .inverse()
            .transform_point(point_2d.extend(0.0))
            .truncate()
    }
    fn transform_rect_from_points(&self, local_pt_a: Vec2, local_pt_b: Vec2) -> Rect {
        let glob_pt_a = self.transform_point2(local_pt_a);
        let glob_pt_b = self.transform_point2(local_pt_b);
        Rect::from_corners(glob_pt_a, glob_pt_b)
    }
    fn inverse_transform_rect_from_points(&self, glob_pt_a: Vec2, glob_pt_b: Vec2) -> Rect {
        let inv_mat = self.compute_matrix().inverse();
        let loc_pt_a = inv_mat.transform_point(glob_pt_a.extend(0.0)).truncate();
        let loc_pt_b = inv_mat.transform_point(glob_pt_b.extend(0.0)).truncate();
        Rect::from_corners(loc_pt_a, loc_pt_b)
    }
    fn extract_transform_scale2(&self) -> Vec2 {
        let origin = self.transform_point2(Vec2::ZERO);
        let axis_x = self.transform_point2(Vec2::new(1.0, 0.0)) - origin;
        let axis_y = self.transform_point2(Vec2::new(0.0, 1.0)) - origin;
        Vec2::new(axis_x.length(), axis_y.length())
    }
}

impl TransformPoint2D for Transform {
    fn transform_point2(&self, point_2d: Vec2) -> Vec2 {
        self.transform_point(point_2d.extend(0.0)).truncate()
    }
    fn inverse_transform_point2(&self, point_2d: Vec2) -> Vec2 {
        self.compute_matrix()
            .inverse()
            .transform_point(point_2d.extend(0.0))
            .truncate()
    }
    fn transform_rect_from_points(&self, local_pt_a: Vec2, local_pt_b: Vec2) -> Rect {
        let glob_pt_a = self.transform_point2(local_pt_a);
        let glob_pt_b = self.transform_point2(local_pt_b);
        Rect::from_corners(glob_pt_a, glob_pt_b)
    }
    fn inverse_transform_rect_from_points(&self, glob_pt_a: Vec2, glob_pt_b: Vec2) -> Rect {
        let inv_mat = self.compute_matrix().inverse();
        let loc_pt_a = inv_mat.transform_point(glob_pt_a.extend(0.0)).truncate();
        let loc_pt_b = inv_mat.transform_point(glob_pt_b.extend(0.0)).truncate();
        Rect::from_corners(loc_pt_a, loc_pt_b)
    }
    fn extract_transform_scale2(&self) -> Vec2 {
        self.scale.truncate()
    }
}

// 2D Rotation: ------------------------------------------------------------------------------------

pub trait Rotation2d {
    /// get the 2d rotation of the object in radians
    fn rotation2d(&self) -> f32;
}

pub trait MutRotation2d {
    /// sets the 2drotation to the specified amount in radians
    fn set_rotation2d(&mut self, rot: f32);
    /// rotates the object by the specified delta in radian
    fn rotate2d(&mut self, delta_rot: f32);
}

impl Rotation2d for Transform {
    fn rotation2d(&self) -> f32 {
        self.rotation.to_euler(EulerRot::XYZ).2
    }
}

impl MutRotation2d for Transform {
    fn set_rotation2d(&mut self, rot: f32) {
        self.rotation = Quat::from_euler(EulerRot::XYZ, 0.0, 0.0, rot);
    }
    fn rotate2d(&mut self, delta_rot: f32) {
        self.set_rotation2d(self.rotation2d() + delta_rot);
    }
}

impl Rotation2d for GlobalTransform {
    fn rotation2d(&self) -> f32 {
        self.compute_transform().rotation2d()
    }
}

pub trait RotateVecQuarterAngle {
    /// rotate the vector by pi/2 radians in the clockwise direction
    fn rotate_cw(&self) -> Self;
    /// rotate the vector by pi/2 radians in the counter-clockwise direction
    fn rotate_ccw(&self) -> Self;
}

impl RotateVecQuarterAngle for Vec2 {
    fn rotate_cw(&self) -> Self {
        Self {
            x: self.y,
            y: -self.x,
        }
    }
    fn rotate_ccw(&self) -> Self {
        Self {
            x: -self.y,
            y: self.x,
        }
    }
}

// 2D Angles: --------------------------------------------------------------------------------------

pub const PI: f32 = std::f32::consts::PI;
pub const TWO_PI: f32 = PI * 2.0;
pub const HALF_PI: f32 = PI * 0.5;

pub trait ToAngle {
    fn to_angle(&self) -> f32;
}

impl ToAngle for Vec2 {
    fn to_angle(&self) -> f32 {
        self.y.atan2(self.x)
    }
}

/// calculate the shortest possible difference between two angles
pub fn signed_angle_dif(from: f32, to: f32) -> f32 {
    let a = to - from;
    modulo(a + PI, TWO_PI) - PI
}
