pub mod controller;
pub mod environment;
pub mod physics;

#[allow(unused)]
pub mod math_utils;

use bevy::{input::InputSystem, prelude::*};

use crate::core::controller::player_cam::PlayerCamBundle;

use self::controller::ControllerPlugin;
use self::environment::EnvironmentPlugin;
use self::physics::PhysicsPlugin;

// System Sets: ------------------------------------------------------------------------------------

/// system set that runs in preupdate directly after [`bevy::input::InputSystem`], which deals
/// with handling player input from mouse, keyboard, controllers, etc
#[derive(SystemSet, Debug, Hash, Clone, Copy, PartialEq, Eq)]
pub struct ControllerSystem;

// Core Plugin: ------------------------------------------------------------------------------------

pub struct CorePlugin;

impl Plugin for CorePlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins((ControllerPlugin, EnvironmentPlugin, PhysicsPlugin))
            .configure_sets(PreUpdate, ControllerSystem.after(InputSystem))
            .add_systems(Startup, setup);
    }
}

// Systems: ----------------------------------------------------------------------------------------

fn setup(mut commands: Commands) {
    // create camera
    commands.spawn(PlayerCamBundle {
        cam_bundle: Camera2dBundle {
            projection: OrthographicProjection {
                near: -1000.0,
                far: 1000.0,
                scale: 0.05,
                ..default()
            },
            ..default()
        },
        ..default()
    });
}
