use std::time::Duration;

use bevy::prelude::*;
use bevy_xpbd_2d::prelude::*;
use bevy_dynamic_gravity_2d::prelude::*;

// Physics Plugin: ---------------------------------------------------------------------------------

pub struct PhysicsPlugin;

impl Plugin for PhysicsPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_plugins((
            PhysicsPlugins::new(FixedUpdate),
            PhysicsDebugPlugin::default(),
            DynamicGravityPlugin
        ))
        .add_systems(Startup, setup);
    }
}

// Utility: ----------------------------------------------------------------------------------------

/// flattens a 2d vector list into a list of inline vector coordinates
fn flatten_vectors(vecs: &Vec<Vec2>) -> Vec<f32> {
    let mut vec = Vec::<f32>::new();
    for vec2 in vecs {
        vec.push(vec2.x);
        vec.push(vec2.y);
    }
    vec
}

/// groups together a list of flattened triangle indices into a list where each triangle is it's
/// own grouped array of 3 indices
fn clump_tris(indices: &Vec<usize>) -> Vec<[u32; 3]> {
    let mut vec = Vec::<[u32; 3]>::new();
    for i in 0..(indices.len() / 3) {
        vec.push([
            indices[i * 3] as u32,
            indices[i * 3 + 1] as u32,
            indices[i * 3 + 2] as u32,
        ]);
    }
    vec
}

/// create a trimesh collider from a list of vector2 vertices
pub fn trimesh_collider(verts: &Vec<Vec2>) -> Collider {
    let tris = earcutr::earcut(&flatten_vectors(verts), &[], 2).unwrap();
    let tris = clump_tris(&tris);
    Collider::trimesh(verts.clone(), tris)
}

// Systems: ----------------------------------------------------------------------------------------

fn setup(
    mut commands: Commands,
    mut phys_dbg_render: ResMut<PhysicsDebugConfig>,
    mut const_gravity: ResMut<Gravity>,
    mut time: ResMut<Time<Virtual>>,
    mut fixed_time: ResMut<Time<Fixed>>,
    mut phys_time: ResMut<Time<Physics>>,
) {
    // set a max timestep for the program
    time.set_max_delta(Duration::from_secs_f64(0.1));

    // determine the fixed and physics timestep delta
    let phys_timestep_delta: Duration = Duration::from_secs_f64(1.0 / 60.0);
    fixed_time.set_timestep(phys_timestep_delta);
    phys_time.set_timestep_mode(TimestepMode::FixedOnce {
        delta: phys_timestep_delta,
    });

    // disable constant gravity because we use dynamic gravity
    const_gravity.0.x = 0.0;
    const_gravity.0.y = 0.0;

    println!("Game Initialized!");
    phys_dbg_render.enabled = true;
    phys_dbg_render.axis_lengths = None;

    warn!("Find better solution to temporary fix for false collision query");
    commands.insert_resource(NarrowPhaseConfig {
        prediction_distance: 0.01,
    });
}
