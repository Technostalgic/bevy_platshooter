use bevy::prelude::*;
use bevy_xpbd_2d::prelude::*;
use bevy_dynamic_gravity_2d::prelude::*;

// Prop --------------------------------------------------------------------------------------------

#[derive(Component, Debug, Clone, Copy)]
pub struct Prop;

#[derive(Bundle)]
pub struct PropBundle {
	pub spatial: SpatialBundle,
	pub collider: Collider,
	pub friction: Friction,
	pub restitution: Restitution,
	pub lin_vel: LinearVelocity,
	pub ang_vel: AngularVelocity,
	dynamic_gravity: DynamicGravity,
	rigid_body: RigidBody,
}

impl Default for PropBundle {
	fn default() -> Self {
		Self {
			spatial: default(),
			collider: Collider::cuboid(1.5, 1.5),
			friction: default(),
			restitution: default(),
			lin_vel: default(),
			ang_vel: default(),
			dynamic_gravity: default(),
			rigid_body: RigidBody::Dynamic,
		}
	}
}

impl PropBundle {
	pub fn new_at(pos: Vec2) -> Self {
		let mut r = Self::default();
		r.spatial.transform.translation = pos.extend(0.0);
		r
	}
	pub fn with_collider(mut self, col: Collider) -> Self {
		self.collider = col;
		self
	}
}