use bevy::{input::InputSystem, prelude::*};
use bevy_xpbd_2d::prelude::*;
use bevy_dynamic_gravity_2d::prelude::*;

use crate::core::{
    controller::{
        action::{Action, ActionReceiver},
        player::PlayerInputSystem,
    },
    ControllerSystem,
};

// Action Delegation System Set: -------------------------------------------------------------------

/// a system set that runs in preupdate after [`bevy::input::InputSystem`], a more
/// precise subset of [`ControllerSystem`] which only contains systems that are dealing directly
/// with input in a way directly related to delegating [`PlayerAction`]s to entities
#[derive(SystemSet, Debug, Hash, Clone, Copy, PartialEq, Eq)]
pub(crate) struct ActionDelegationSystem;

// Hero Plugin: ------------------------------------------------------------------------------------

pub struct HeroPlugin;

impl Plugin for HeroPlugin {
    fn build(&self, app: &mut App) {
        app.configure_sets(
            PreUpdate,
            ActionDelegationSystem
                .after(InputSystem)
                .after(PlayerInputSystem),
        )
        .add_systems(
            PreUpdate,
            parse_hero_actions
                .in_set(ActionDelegationSystem)
                .in_set(ControllerSystem),
        )
        .add_systems(FixedUpdate, hero_actions.before(GroundMovement));
    }
}

// Hero: -------------------------------------------------------------------------------------------

#[derive(Component, Default, Clone, Copy)]
pub struct Hero {
    pub player_id: usize,
    movement: Vec2,
    is_attacking: bool,
    is_interacting: bool,
}

#[derive(Bundle)]
pub struct HeroBundle {
    pub spatial: SpatialBundle,
    pub hero: Hero,
    pub action_receiver: ActionReceiver,
    pub collider: Collider,
    pub orient: OrientToGravity,
    pub ground_jumper: GroundJumper,
    pub ground_mover: GroundMover,
    grounded: Grounded,
    dynamic_gravity: DynamicGravity,
    rigid_body: RigidBody,
    pub gravity_scale: GravityScale,
    friction: Friction,
    restitution: Restitution,
    pub lin_vel: LinearVelocity,
    locked_axes: LockedAxes,
    sleep_disable: SleepingDisabled,
}

impl Default for HeroBundle {
    fn default() -> Self {
        Self {
            spatial: default(),
            hero: default(),
            action_receiver: default(),
            collider: Collider::capsule(1.0, 0.5),
            orient: default(),
            grounded: default(),
            ground_jumper: default(),
            ground_mover: default(),
            dynamic_gravity: default(),
            rigid_body: RigidBody::Dynamic,
            gravity_scale: GravityScale(3.0),
            friction: Friction::new(0.0),
            restitution: Restitution::new(0.0),
            lin_vel: LinearVelocity::ZERO,
            locked_axes: LockedAxes::ROTATION_LOCKED,
            sleep_disable: SleepingDisabled,
        }
    }
}

// Systems: ----------------------------------------------------------------------------------------

fn parse_hero_actions(
    mut query: Query<(
        &mut ActionReceiver,
        &mut Hero,
        &mut GroundMover,
        &mut GroundJumper,
    )>,
) {
    for (mut act_rec, mut hero, mut mover, mut jumper) in &mut query {
        let mut is_jumping = false;
        for action in act_rec.recieved_actions() {
            match action {
                Action::MoveHorizontal(mov) => {
                    hero.movement.x += mov.0;
                }
                Action::MoveVertical(mov) => {
                    hero.movement.y += mov.0;
                }
                Action::Jump => {
                    is_jumping = true;
                }
                Action::Interact => {
                    hero.is_interacting = true;
                }
                Action::Attack => {
                    hero.is_attacking = true;
                }
            }
        }
        act_rec.clear_actions();
        // signal other components for actions
        mover.set_absolute_movement(hero.movement);
        jumper.set_jumping(is_jumping);
    }
}

fn hero_actions(mut hero_query: Query<&mut Hero>) {
    for mut hero in &mut hero_query {
        if hero.is_interacting {
            warn!("implement interaction");
        }
        if hero.is_attacking {
            warn!("implement attack");
        }
        // reset action flags
        hero.movement.x = 0.0;
        hero.movement.y = 0.0;
        hero.is_interacting = false;
        hero.is_attacking = false;
    }
}
