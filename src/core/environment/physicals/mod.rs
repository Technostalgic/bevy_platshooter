use bevy::app::Plugin;

pub mod hero;
pub mod props;

// Physicals Plugin: -------------------------------------------------------------------------------

pub struct PhysicalsPlugin;

impl Plugin for PhysicalsPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_plugins((
            hero::HeroPlugin,
        ));
    }
}
