use bevy::prelude::*;
use bevy_xpbd_2d::prelude::*;
use rand::prelude::*;

pub mod level;
pub mod physicals;

use self::physicals::{hero::HeroBundle, props::PropBundle};

// Environment Plugin: -----------------------------------------------------------------------------

pub struct EnvironmentPlugin;

impl Plugin for EnvironmentPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app.add_plugins((level::LevelPlugin, physicals::PhysicalsPlugin))
            .add_systems(Startup, setup);
    }
}

// Systems: ----------------------------------------------------------------------------------------

fn setup(mut commands: Commands) {
    // create player
    commands.spawn(HeroBundle::default());

    // create props:
    let ctr = Vec2::new(0.0, 15.0);
    for i in 0..50 {
        let off = Vec2::new(
            rand::thread_rng().gen_range(-1.0..=1.0),
            rand::thread_rng().gen_range(-1.0..=1.0),
        ) * 10.0;
        let col = if i % 2 == 0 {
            Collider::ball(0.45)
        } else {
            Collider::cuboid(1.5, 1.0)
        };
        commands.spawn(PropBundle::new_at(off + ctr).with_collider(col));
    }
}
