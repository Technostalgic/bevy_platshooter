use bevy::{ecs::system::SystemId, prelude::*};
use bevy_poly_level::prelude::*;
use bevy_xpbd_2d::prelude::*;
use bevy_dynamic_gravity_2d::prelude::*;

use crate::core::physics::trimesh_collider;

// System Sets: ------------------------------------------------------------------------------------

#[derive(SystemSet, Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct LevelLoading;

// Level Plugin: -----------------------------------------------------------------------------------

pub struct LevelPlugin;

impl Plugin for LevelPlugin {
    fn build(&self, app: &mut App) {
        let sys_ids = LevelSysIds {
            clear_level: app.world.register_system(clear_game_level),
        };
        app.configure_sets(PostUpdate, LevelLoading)
            .insert_resource(sys_ids)
            .insert_resource(LevelLoader::from_loading_level(
                "test_level.lvl.json".to_string(),
            ))
            .add_systems(PostUpdate, load_level.in_set(LevelLoading))
            .add_systems(OnEnter(LevelEditorState::Editing), on_edit_begin)
            .add_systems(OnEnter(LevelEditorState::Off), on_edit_end);
    }
}

// Oneshot System Ids: -----------------------------------------------------------------------------

/// container for all the oneshot systems in the level module
#[derive(Resource, Clone, Copy)]
pub struct LevelSysIds {
    /// clear all entities from the world that are part of the currently loaded level
    pub clear_level: SystemId,
}

// Level Objects: ----------------------------------------------------------------------------------

/// a marker component that should be used on every object which is part of the currently loaded
/// level
#[derive(Component, Clone, Copy)]
pub struct LevelObject;

// Level Geometry: ---------------------------------------------------------------------------------

/// a level geometry marker component, generally used for static or kinematic level collision
/// entities
#[derive(Component, Clone, Copy)]
pub struct LevelGeometry;

// Level Loader: -----------------------------------------------------------------------------------

#[derive(Resource, Default, Debug, Clone)]
pub struct LevelLoader {
    /// the path pointing to the level asset that should be loaded
    pub asset_path: String,
    asset_handle: Handle<LevelData>,
    /// whether or not the level at the specified path should load when the system executes
    do_load: bool,
    do_reset: bool,
    create_level: bool,
    set_editor_data: bool,
}

impl LevelLoader {
    /// create a new level loader with the instruction to load the specified level asset
    pub fn from_loading_level(asset_path: String) -> Self {
        Self {
            asset_path,
            do_load: true,
            create_level: false,
            ..Default::default()
        }
    }
    /// tell the loader to load the specified level asset
    pub fn _load_level_at(&mut self, asset_path: String) {
        self.asset_path = asset_path;
        self.do_load = true;
    }
    /// tell the loader to reset the currently loaded level
    pub fn _reset_level(&mut self) {
        self.do_reset = true;
    }
}

/// load the level and clear
fn load_level(
    mut commands: Commands,
    sys_ids: Res<LevelSysIds>,
    asset_server: Res<AssetServer>,
    level_assets: Res<Assets<LevelData>>,
    mut loader: ResMut<LevelLoader>,
    mut editor_data: ResMut<LevelEditorData>,
) {
    if loader.do_load {
        loader.do_load = false;
        loader.create_level = true;
        loader.set_editor_data = true;
        commands.run_system(sys_ids.clear_level);
        loader.asset_handle = asset_server.load(loader.asset_path.clone());
    } else if loader.do_reset {
        if editor_data.level_data.is_none() {
            warn!("No level data to reset to");
        } else {
            loader.do_reset = false;
            loader.create_level = true;
            commands.run_system(sys_ids.clear_level);
        }
    } else if loader.create_level {
        if let Some(level_data) = level_assets.get(&loader.asset_handle) {
            if loader.set_editor_data {
                editor_data.level_data = Some(level_data.clone());
                loader.set_editor_data = false;
            }
            create_level(&mut commands, level_data);
            loader.create_level = false;
        }
    }
}

// Utility: ----------------------------------------------------------------------------------------

fn create_level(commands: &mut Commands, level_data: &LevelData) {
    // iterate through each level object in the editor level data and instantiate it
    for poly_data in &level_data.poly_data.polys {
        // create physics collision entity
        commands.spawn((
            LevelObject,
            LevelGeometry,
            DynamicGravityAttractor::default(), // create a gravity attractor on each polygon
            SpatialBundle::from_transform(Transform::from_translation(
                poly_data.translation.extend(0.0),
            )),
            trimesh_collider(&poly_data.verts),
            RigidBody::Static,
        ));
    }
}

// Systems: ----------------------------------------------------------------------------------------

fn clear_game_level(mut commands: Commands, ent_query: Query<Entity, With<LevelObject>>) {
    for ent in &ent_query {
        commands.entity(ent).despawn();
    }
}

/// called whenever the level editor switches to edit mode
fn on_edit_begin() {
    println!("Began level editing");
}

// called when the level editor leaves edit mode
fn on_edit_end(
    mut commands: Commands,
    lvl_sys_ids: Res<LevelSysIds>,
    editor_data: Res<LevelEditorData>,
) {
    println!("Finished level editing");
    // clear the level entities from the world
    commands.run_system(lvl_sys_ids.clear_level);
    // iterate through each level object in the editor level data and instantiate it
    if let Some(lvl_dat) = &editor_data.level_data {
        create_level(&mut commands, lvl_dat);
    }
}
