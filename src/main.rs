mod core;

use bevy::prelude::*;
use bevy_poly_level::prelude::*;
use bevy_screen_diagnostics::{
    ScreenDiagnosticsPlugin, ScreenEntityDiagnosticsPlugin, ScreenFrameDiagnosticsPlugin,
};
use core::CorePlugin;

// ------------------------------------------------------------------------------------------------

fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins,
            ScreenDiagnosticsPlugin::default(),
            ScreenFrameDiagnosticsPlugin,
            ScreenEntityDiagnosticsPlugin,
            BevyLevelEditPlugin::new(true, false, Some(KeyCode::F5)),
            CorePlugin,
        ))
        .run();
}
